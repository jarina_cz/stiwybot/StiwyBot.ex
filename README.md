# StiwyBot

Simple bot for Discord built on top of https://hex.pm/packages/discord_alchemy

## Installation

```bash
git clone https://github.com/stefanjarina/stiwy_bot_elixir
cd stiwy_bot_elixir

# Linux/Mac OS - bash
export DISCORD_BOT_TOKEN_STIWY_BOT="your_discord_app_token"

# Windows - PowerShell
$Env:DISCORD_BOT_TOKEN_STIWY_BOT="your_discord_app_token"
```

## Run in development

```
# Start as a standalone process
mix run --no-halt

# Start In EIX console
iex -S mix
```
* Starting the application in the repl is very advantageous, as it allows you to interact with the bot live.

## Release for production

#### Configure

* You can configure release in `rel/config.exs`

#### Build

* Production build is set to include also erlang so there is no need to install erlang on targeted system.

```bash
mix compile
MIX_ENV=prod mix release
```

#### Use

* We need to include `REPLACE_OS_VARS=true` because application is using environment variables for security.

```bash
cd _build/prod/rel/stiwy_bot/bin

# Interactive:
REPLACE_OS_VARS=true stiwy_bot console
# Foreground:
REPLACE_OS_VARS=true stiwy_bot foreground
# Daemon:
REPLACE_OS_VARS=true stiwy_bot start

```

## Known Issues

* I had problems to start this bot with OTP 20 and **had to use OTP 19.x**
  * I think this is a problem of dependencies that some of the packages are using and they are not yet upgraded for OTP 20

## TODO

* [x] Distillery package to be able to run it as a standalone service on a server
* [ ] Tests

## Contributors

none

## Author

Stefan Jarina <stefan@jarina.cz>
