defmodule StiwyBot.Fun do
  @moduledoc """
  StiwyBot module to fetch comics from https://xkcd.com/
  """

  use Alchemy.Cogs

  @doc """
  Fetches random comic
  """
  Cogs.def comic do
    {:ok, %{ img: img }} = Xkcd.random()
    Cogs.say(img)
  end

  @doc """
  Fetches latest comic
  """
  Cogs.def comic("latest") do
    {:ok, %{ img: img }} = Xkcd.latest()
    Cogs.say(img)
  end

  @doc """
  Fetches specified comic
  """
  Cogs.def comic(args) do
    case Integer.parse(args) do
      {num, _} ->
        {:ok, %{ img: img }} = Xkcd.number(num)
        Cogs.say(img)
      :error ->
        Cogs.say("Argument to ;comic must be a number or 'latest'")
    end
  end

end
