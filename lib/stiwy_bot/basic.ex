defmodule StiwyBot.Basic do
  use Alchemy.Cogs
  alias Alchemy.{Client}
  alias StiwyBot.Util.Time

  @doc """
  The classic ping command, including the latency.
  """
  Cogs.def ping do
    # message is an implicit parameter to commands
    old = message.timestamp
    IO.inspect(old)
    {:ok, message} = Cogs.say("pong!")
    time = Time.diff(message.timestamp, old)
    Client.edit_message(message, message.content <> "\ntook #{time} ms")
  end

end
