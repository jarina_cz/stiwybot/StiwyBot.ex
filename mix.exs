defmodule StiwyBot.Mixfile do
  use Mix.Project

  def project do
    [
      app: :stiwy_bot,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps(),

      # Docs
      name: "StiwyBot",
      source_url: "https://github.com/stefanjarina/stiwy_bot_elixir",
      homepage_url: "https://github.com/stefanjarina/stiwy_bot_elixir",
      docs: [main: "StiwyBot",
        #logo: "path/to/logo.png",
        extras: ["README.md"]]
    ]
  end

  def application do
    [
      mod: {StiwyBot, []},
      extra_applications: [:logger, :xkcd]
    ]
  end

  defp aliases do
    [test: "test --no-start"]
  end

  defp deps do
    [
      {:distillery, "~> 1.4", runtime: false},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false},
      {:alchemy, "~> 0.6.0", hex: :discord_alchemy},
      {:httpoison, "~> 0.13", override: true},
      {:xkcd, "~> 0.0.3"},
    ]
  end
end
